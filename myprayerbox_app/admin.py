from django.contrib import admin
from myprayerbox_app.models import *

admin.site.register(Country)
admin.site.register(State)
admin.site.register(City)
admin.site.register(Token)
admin.site.register(RequestValue)
admin.site.register(ChurchType)
admin.site.register(Church)
admin.site.register(Pastor)
admin.site.register(MpbUser)
admin.site.register(MpbAddress)
admin.site.register(PastorAddress)
admin.site.register(ChurchAddress)
admin.site.register(RequestText)
admin.site.register(RequestSound)
admin.site.register(ChurchRequest)
admin.site.register(Response)
admin.site.register(DropAccount)
admin.site.register(Transaction)
admin.site.register(BlackList)
admin.site.register(BuyToken)
admin.site.register(SpendToken)
admin.site.register(RequestCount)
admin.site.register(RandomChurchCount)
admin.site.register(FavoriteChurch)
admin.site.register(LikeChurch)
admin.site.register(Help)
admin.site.register(Privacy)
admin.site.register(Terms)


# Register your models here.
