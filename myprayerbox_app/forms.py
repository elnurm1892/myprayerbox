from django.core.exceptions import ValidationError
from myprayerbox_app.models import *
from django.forms import ModelForm
from django import forms
from myprayerbox_app.models import MpbUser
from django.forms import ModelForm
from django.contrib.auth.forms import AuthenticationForm
from django import forms


class SearchForm(forms.Form):
    search = forms.CharField()

    def clean_search(self):
        key = self.cleaned_data['search']
        if len(key) < 3:
            raise forms.ValidationError("Search word must be less than 3 symbols")
        return key


class MpbUserForm(ModelForm):
    class Meta:
        model = MpbUser
        exclude = ('id', 'user', 'activation_key', 'expiration_date')


class RegistrationForm(forms.Form):
    email = forms.EmailField()
    name = forms.CharField()
    surname = forms.CharField()
    password = forms.CharField(widget=forms.PasswordInput)
    password2 = forms.CharField(widget=forms.PasswordInput)

    def clean_password2(self):
        password = self.cleaned_data.get("password")
        password2 = self.cleaned_data.get("password2")
        if password and password2 and password != password2:
            raise forms.ValidationError("Password doesn't match!")
        return password2

    def clean_email(self):
        email = self.cleaned_data.get("email")
        if User.objects.filter(email__iexact=email):
            raise forms.ValidationError("Sorry, it looks like "+email+
                                        " belongs to an existing account. ")
        return email


class ChurchRequestForm(forms.Form):
    STATUS_CHOICE = (
        ('pending', 'pending'),
        ('view', 'view'),
        ('confirmed', 'confirmed'),
        ('cancel', 'cancel'),
        ('responded', 'responded'),
    )
    status = forms.ChoiceField(choices=STATUS_CHOICE)

    exclude = ('id', 'user', 'activation_key', 'date')


class CreateRequestForm(forms.Form):
    TYPE_CHOICE = (
        ('without_money', 'Free'),
        ('with_money', 'Paid')
    )

    title = forms.CharField()
    text = forms.CharField(widget=forms.Textarea)
    type = forms.ChoiceField(choices=TYPE_CHOICE)


class DropAccountForm(forms.Form):
    reason = forms.CharField(widget=forms.Textarea)


class RateForm(forms.Form):
    POINT_CHOICES = ((1, 1), (2, 2), (3, 3), (4, 4), (5, 5))
    point = forms.ChoiceField(choices=POINT_CHOICES)


class AddressForm(ModelForm):
    class Meta:
        model = MpbAddress
        exclude = ('owner',)


class BlackListForm(ModelForm):
    class Meta:
        model = BlackList
        fields = ('reason',)


class PastorForm(forms.ModelForm):
    class Meta:
        model = Pastor
        fields = ('image', 'notification')


class BlackListForm(forms.ModelForm):
    class Meta:
         model = BlackList
         exclude = ('church', 'user', 'date',)
