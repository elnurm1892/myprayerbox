# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='BlackList',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('reason', models.TextField(max_length=200)),
                ('date', models.DateField(auto_now=True)),
            ],
        ),
        migrations.CreateModel(
            name='BuyToken',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('date', models.DateField(auto_now=True)),
            ],
        ),
        migrations.CreateModel(
            name='Church',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=50, verbose_name='Church name')),
                ('country_church_id', models.CharField(max_length=15, verbose_name='Country id')),
                ('image', models.ImageField(upload_to='', null=True, verbose_name='Church profile image', blank=True)),
                ('date', models.DateField(auto_now=True)),
                ('bgfoto', models.ImageField(null=True, upload_to='', blank=True)),
                ('description', models.TextField(null=True, blank=True)),
                ('tel', models.CharField(max_length=20)),
            ],
            options={
                'ordering': ['name'],
            },
        ),
        migrations.CreateModel(
            name='ChurchAddress',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('street', models.CharField(max_length=100, blank=True)),
            ],
        ),
        migrations.CreateModel(
            name='ChurchRequest',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('status', models.CharField(default='pending', max_length=10, choices=[('pending', 'pending'), ('view', 'view'), ('confirmed', 'confirmed'), ('cancel', 'cancel'), ('responded', 'responded')])),
                ('date', models.DateField(auto_now=True)),
                ('church', models.ForeignKey(to='myprayerbox_app.Church')),
            ],
        ),
        migrations.CreateModel(
            name='ChurchRequestSound',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('status', models.CharField(default='pending', max_length=10, choices=[('pending', 'pending'), ('view', 'view'), ('confirmed', 'confirmed'), ('cancel', 'cancel'), ('responded', 'responded')])),
                ('date', models.DateField(auto_now=True)),
                ('church', models.ForeignKey(to='myprayerbox_app.Church')),
            ],
        ),
        migrations.CreateModel(
            name='ChurchType',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(unique=True, max_length=30, verbose_name='Church type')),
                ('sup_church_type', models.OneToOneField(null=True, blank=True, to='myprayerbox_app.ChurchType', verbose_name='Super church type')),
            ],
            options={
                'ordering': ['name'],
            },
        ),
        migrations.CreateModel(
            name='City',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=30, verbose_name='City name')),
            ],
            options={
                'ordering': ['name'],
            },
        ),
        migrations.CreateModel(
            name='Country',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(unique=True, max_length=30, verbose_name='Country name')),
            ],
            options={
                'ordering': ['name'],
            },
        ),
        migrations.CreateModel(
            name='DropAccount',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('reason', models.TextField(max_length=100, null=True, blank=True)),
                ('date', models.DateField(auto_now=True)),
            ],
        ),
        migrations.CreateModel(
            name='FavoriteChurch',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('date', models.DateField(auto_now=True)),
                ('point', models.IntegerField(choices=[(1, 1), (2, 2), (3, 3), (4, 4), (5, 5)])),
                ('church', models.ForeignKey(to='myprayerbox_app.Church')),
            ],
        ),
        migrations.CreateModel(
            name='Help',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('question', models.TextField()),
                ('answer', models.TextField()),
            ],
        ),
        migrations.CreateModel(
            name='LikeChurch',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('date', models.DateField(auto_now=True)),
                ('church', models.ForeignKey(to='myprayerbox_app.Church')),
            ],
        ),
        migrations.CreateModel(
            name='MpbAddress',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('street', models.CharField(max_length=60, null=True, blank=True)),
                ('city', models.ForeignKey(blank=True, to='myprayerbox_app.City', null=True)),
                ('country', models.ForeignKey(blank=True, to='myprayerbox_app.Country', null=True)),
            ],
        ),
        migrations.CreateModel(
            name='MpbUser',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('activation_key', models.CharField(max_length=500, blank=True)),
                ('expiration_date', models.DateTimeField(default=datetime.datetime(2015, 8, 3, 11, 14, 10, 454007))),
                ('first_name', models.CharField(max_length=25, verbose_name='First name')),
                ('last_name', models.CharField(max_length=25, verbose_name='Last name')),
                ('gender', models.CharField(blank=True, max_length=6, null=True, verbose_name='Gender', choices=[('Male', 'Male'), ('Female', 'Female')])),
                ('birth_date', models.DateField(null=True, verbose_name='Birth date', blank=True)),
                ('image', models.ImageField(upload_to='', null=True, verbose_name='Profile image', blank=True)),
                ('phone_number', models.CharField(max_length=16, null=True, verbose_name='Phone number', blank=True)),
                ('cellular_number', models.CharField(max_length=16, null=True, verbose_name='Cellular number', blank=True)),
                ('email', models.EmailField(max_length=254, verbose_name='Email')),
                ('date', models.DateField(auto_now=True)),
                ('notification', models.BooleanField(default=True)),
                ('user', models.OneToOneField(to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'ordering': ['first_name', 'last_name'],
            },
        ),
        migrations.CreateModel(
            name='Pastor',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('first_name', models.CharField(max_length=25, verbose_name='First name')),
                ('last_name', models.CharField(max_length=25, verbose_name='Last name')),
                ('birth_date', models.DateField(null=True, verbose_name='Birth date', blank=True)),
                ('phone_number', models.CharField(max_length=16, null=True, blank=True)),
                ('cellular_number', models.CharField(max_length=16, null=True, blank=True)),
                ('email', models.EmailField(max_length=254)),
                ('image', models.ImageField(null=True, upload_to='', blank=True)),
                ('notification', models.BooleanField(default=True)),
                ('date', models.DateField(auto_now=True)),
                ('church', models.ForeignKey(to='myprayerbox_app.Church')),
                ('user', models.OneToOneField(to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'ordering': ['first_name', 'last_name'],
            },
        ),
        migrations.CreateModel(
            name='PastorAddress',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('street', models.TextField(max_length=100, null=True, blank=True)),
                ('city', models.ForeignKey(blank=True, to='myprayerbox_app.City', null=True)),
                ('country', models.ForeignKey(blank=True, to='myprayerbox_app.Country', null=True)),
                ('owner', models.OneToOneField(to='myprayerbox_app.Pastor')),
            ],
        ),
        migrations.CreateModel(
            name='Privacy',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('content', models.TextField()),
            ],
        ),
        migrations.CreateModel(
            name='RandomChurchCount',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('count', models.PositiveIntegerField(unique=True)),
                ('status', models.NullBooleanField()),
                ('date', models.DateField(auto_now=True)),
            ],
            options={
                'ordering': ['count'],
            },
        ),
        migrations.CreateModel(
            name='RequestCount',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('count', models.PositiveIntegerField(unique=True)),
                ('status', models.NullBooleanField()),
                ('date', models.DateField(auto_now=True)),
            ],
            options={
                'ordering': ['count'],
            },
        ),
        migrations.CreateModel(
            name='RequestSound',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=15, null=True, blank=True)),
                ('sound', models.FileField(upload_to='files/sound')),
                ('type', models.CharField(max_length=20, choices=[('with_money', 'with_money'), ('without_money', 'without_money')])),
                ('date', models.DateField(auto_now=True)),
                ('status', models.CharField(default='pending', max_length=12, verbose_name='Status')),
                ('church', models.ForeignKey(verbose_name='Church', blank=True, to='myprayerbox_app.Church', null=True)),
                ('user', models.ForeignKey(to='myprayerbox_app.MpbUser')),
            ],
        ),
        migrations.CreateModel(
            name='RequestText',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=15, verbose_name='Title', blank=True)),
                ('text', models.TextField(max_length=320, verbose_name='Request text')),
                ('type', models.CharField(max_length=20, choices=[('with_money', 'with_money'), ('without_money', 'without_money')])),
                ('date', models.DateField(auto_now=True)),
                ('status', models.CharField(default='pending', max_length=12, verbose_name='Status')),
                ('church', models.ForeignKey(verbose_name='Church', blank=True, to='myprayerbox_app.Church', null=True)),
                ('user', models.ForeignKey(to='myprayerbox_app.MpbUser')),
            ],
        ),
        migrations.CreateModel(
            name='RequestValue',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('value', models.PositiveIntegerField(unique=True, verbose_name='Request value')),
                ('status', models.NullBooleanField()),
                ('date', models.DateField(auto_now=True)),
            ],
            options={
                'ordering': ['value'],
            },
        ),
        migrations.CreateModel(
            name='Response',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('text', models.TextField(verbose_name='Response text')),
                ('sound_response', models.FileField(null=True, upload_to='files/sound', blank=True)),
                ('date', models.DateField(auto_now=True)),
                ('church', models.ForeignKey(to='myprayerbox_app.Church')),
                ('pastor', models.ForeignKey(to='myprayerbox_app.Pastor')),
                ('request', models.OneToOneField(to='myprayerbox_app.ChurchRequest')),
            ],
        ),
        migrations.CreateModel(
            name='SpendToken',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('request_value', models.PositiveIntegerField(blank=True)),
                ('date', models.DateField(auto_now=True)),
                ('user', models.ForeignKey(to='myprayerbox_app.MpbUser')),
            ],
        ),
        migrations.CreateModel(
            name='State',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=30, verbose_name='State name')),
                ('country', models.ForeignKey(to='myprayerbox_app.Country')),
            ],
            options={
                'ordering': ['name'],
            },
        ),
        migrations.CreateModel(
            name='Terms',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('content', models.TextField()),
            ],
        ),
        migrations.CreateModel(
            name='Token',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('count', models.PositiveIntegerField(unique=True, verbose_name='Count of token')),
                ('cost', models.PositiveIntegerField(unique=True, verbose_name='Cost of token')),
                ('date', models.DateField(auto_now=True)),
            ],
            options={
                'ordering': ['cost'],
            },
        ),
        migrations.CreateModel(
            name='Transaction',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('status', models.CharField(max_length=20)),
                ('log', models.TextField(max_length=200)),
                ('date', models.DateField(auto_now=True)),
                ('user', models.ForeignKey(to='myprayerbox_app.MpbUser')),
            ],
        ),
        migrations.AddField(
            model_name='pastoraddress',
            name='state',
            field=models.ForeignKey(blank=True, to='myprayerbox_app.State', null=True),
        ),
        migrations.AddField(
            model_name='mpbaddress',
            name='owner',
            field=models.OneToOneField(to='myprayerbox_app.MpbUser'),
        ),
        migrations.AddField(
            model_name='mpbaddress',
            name='state',
            field=models.ForeignKey(blank=True, to='myprayerbox_app.State', null=True),
        ),
        migrations.AddField(
            model_name='likechurch',
            name='user',
            field=models.ForeignKey(to='myprayerbox_app.MpbUser'),
        ),
        migrations.AddField(
            model_name='favoritechurch',
            name='user',
            field=models.ForeignKey(to='myprayerbox_app.MpbUser'),
        ),
        migrations.AddField(
            model_name='dropaccount',
            name='user',
            field=models.OneToOneField(to='myprayerbox_app.MpbUser'),
        ),
        migrations.AddField(
            model_name='city',
            name='country',
            field=models.ForeignKey(verbose_name='Country', to='myprayerbox_app.Country'),
        ),
        migrations.AddField(
            model_name='city',
            name='state',
            field=models.ForeignKey(verbose_name='State', to='myprayerbox_app.State'),
        ),
        migrations.AddField(
            model_name='churchrequestsound',
            name='request',
            field=models.ForeignKey(to='myprayerbox_app.RequestSound'),
        ),
        migrations.AddField(
            model_name='churchrequest',
            name='request',
            field=models.ForeignKey(to='myprayerbox_app.RequestText'),
        ),
        migrations.AddField(
            model_name='churchaddress',
            name='city',
            field=models.ForeignKey(to='myprayerbox_app.City', blank=True),
        ),
        migrations.AddField(
            model_name='churchaddress',
            name='country',
            field=models.ForeignKey(to='myprayerbox_app.Country', blank=True),
        ),
        migrations.AddField(
            model_name='churchaddress',
            name='owner',
            field=models.OneToOneField(to='myprayerbox_app.Church'),
        ),
        migrations.AddField(
            model_name='churchaddress',
            name='state',
            field=models.ForeignKey(to='myprayerbox_app.State', blank=True),
        ),
        migrations.AddField(
            model_name='church',
            name='church_type',
            field=models.ForeignKey(verbose_name='Church type', to='myprayerbox_app.ChurchType'),
        ),
        migrations.AddField(
            model_name='buytoken',
            name='token',
            field=models.ForeignKey(to='myprayerbox_app.Token'),
        ),
        migrations.AddField(
            model_name='buytoken',
            name='user',
            field=models.ForeignKey(to='myprayerbox_app.MpbUser'),
        ),
        migrations.AddField(
            model_name='blacklist',
            name='church',
            field=models.ForeignKey(to='myprayerbox_app.Church'),
        ),
        migrations.AddField(
            model_name='blacklist',
            name='user',
            field=models.ForeignKey(to='myprayerbox_app.MpbUser'),
        ),
        migrations.AlterUniqueTogether(
            name='response',
            unique_together=set([('church', 'request')]),
        ),
        migrations.AlterUniqueTogether(
            name='churchrequestsound',
            unique_together=set([('church', 'request')]),
        ),
        migrations.AlterUniqueTogether(
            name='blacklist',
            unique_together=set([('church', 'user')]),
        ),
    ]
