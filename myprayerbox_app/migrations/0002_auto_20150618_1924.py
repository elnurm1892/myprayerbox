# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('myprayerbox_app', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='church',
            name='tel',
            field=models.CharField(max_length=20),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='mpbuser',
            name='expiration_date',
            field=models.DateTimeField(default=datetime.datetime(2015, 6, 18, 19, 24, 44, 562432)),
            preserve_default=True,
        ),
    ]
