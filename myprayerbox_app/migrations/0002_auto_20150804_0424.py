# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('myprayerbox_app', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='mpbuser',
            name='expiration_date',
            field=models.DateTimeField(default=datetime.datetime(2015, 8, 4, 4, 24, 3, 962634)),
        ),
    ]
