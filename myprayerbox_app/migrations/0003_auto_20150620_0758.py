# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('myprayerbox_app', '0002_auto_20150618_1924'),
    ]

    operations = [
        migrations.CreateModel(
            name='Terms',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, primary_key=True, serialize=False)),
                ('content', models.TextField()),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AlterField(
            model_name='mpbuser',
            name='expiration_date',
            field=models.DateTimeField(default=datetime.datetime(2015, 6, 20, 7, 58, 20, 177543)),
            preserve_default=True,
        ),
    ]
