# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('myprayerbox_app', '0002_auto_20150804_0424'),
    ]

    operations = [
        migrations.AlterField(
            model_name='mpbuser',
            name='expiration_date',
            field=models.DateTimeField(default=datetime.datetime(2015, 8, 14, 12, 21, 12, 617806)),
        ),
        migrations.AlterField(
            model_name='requesttext',
            name='title',
            field=models.CharField(max_length=150, verbose_name='Title', blank=True),
        ),
    ]
