# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('myprayerbox_app', '0003_auto_20150620_0758'),
    ]

    operations = [
        migrations.AlterField(
            model_name='mpbuser',
            name='expiration_date',
            field=models.DateTimeField(default=datetime.datetime(2015, 6, 27, 17, 4, 23, 839783)),
            preserve_default=True,
        ),
    ]
