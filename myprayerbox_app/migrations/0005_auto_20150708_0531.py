# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('myprayerbox_app', '0004_auto_20150627_1704'),
    ]

    operations = [
        migrations.AlterField(
            model_name='mpbuser',
            name='expiration_date',
            field=models.DateTimeField(default=datetime.datetime(2015, 7, 8, 5, 31, 22, 322256)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='requestsound',
            name='church',
            field=models.ForeignKey(blank=True, to='myprayerbox_app.Church', null=True, verbose_name='Church'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='requesttext',
            name='church',
            field=models.ForeignKey(blank=True, to='myprayerbox_app.Church', null=True, verbose_name='Church'),
            preserve_default=True,
        ),
    ]
