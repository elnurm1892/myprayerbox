from django.core.exceptions import ValidationError
from django.db import models
from django.contrib.auth.models import User
import uuidfield
import datetime
import os
from myprayerbox_pro.settings import *
from django.conf import settings
import uuid
from django.core.exceptions import ValidationError

class Country(models.Model):
    name = models.CharField(max_length=30, unique=True, verbose_name='Country name')

    def __str__(self):
        return self.name

    class Meta:
        ordering = ['name']


class State(models.Model):
    name = models.CharField(max_length=30, verbose_name='State name')
    country = models.ForeignKey(Country)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ['name']


class City(models.Model):
    name = models.CharField(max_length=30, verbose_name='City name')
    country = models.ForeignKey(Country, verbose_name='Country')
    state = models.ForeignKey(State, verbose_name='State')

    def __str__(self):
        return self.name

    class Meta:
        ordering = ['name']


# Tokenin naminal classi
class Token(models.Model):
    count = models.PositiveIntegerField(unique=True, verbose_name='Count of token')
    cost = models.PositiveIntegerField(unique=True, verbose_name='Cost of token')
    date = models.DateField(auto_now=True)
    def __str__(self):
        return str(self.count) + " : "+str(self.cost) + "$"

    class Meta:
        ordering = ['cost']


# Requestin deyeri classi
class RequestValue(models.Model):
    value = models.PositiveIntegerField(unique=True, verbose_name='Request value')
    status = models.NullBooleanField(blank=True)
    date = models.DateField(auto_now=True)

    def __str__(self):
        return str(self.value)

    def save(self, *args, **kwargs):
        RequestValue.objects.filter(status=True).update(status=False)
        super(RequestValue, self).save(*args, **kwargs)

    class Meta:
        ordering = ['value']


class ChurchType(models.Model):
    name = models.CharField(unique=True, max_length=30, verbose_name='Church type')
    sup_church_type = models.OneToOneField('self', null=True, blank=True, verbose_name='Super church type')

    def __str__(self):
        return self.name

    class Meta:
        ordering = ['name']


class Church(models.Model):
    name = models.CharField(max_length=50, verbose_name='Church name')
    country_church_id = models.CharField(max_length=15, verbose_name='Country id')
    church_type = models.ForeignKey(ChurchType, verbose_name='Church type')
    image = models.ImageField(blank=True, null=True, verbose_name='Church profile image')
    date = models.DateField(auto_now=True)
    bgfoto = models.ImageField(blank=True, null=True)
    description = models.TextField(blank=True, null=True)
    tel = models.CharField(max_length=20)

    def __str__(self):
        return self.name

    @property
    def get_image(self):
        return self.image.url if self.image and hasattr(self.image, 'url') else "/media/church.jpg"

    @property
    def get_bgimage(self):
        return self.bgfoto.url if self.bgfoto and hasattr(self.bgfoto, 'url') else "/media/bgfoto.jpg"

    class Meta:
        ordering = ['name']


class Pastor(models.Model):
    user = models.OneToOneField(User)
    first_name = models.CharField(max_length=25, verbose_name='First name')
    last_name = models.CharField(max_length=25, verbose_name='Last name')
    birth_date = models.DateField(blank=True, null=True, verbose_name='Birth date')
    church = models.ForeignKey(Church)
    phone_number = models.CharField(max_length=16, blank=True, null=True)
    cellular_number = models.CharField(max_length=16, blank=True, null=True)
    email = models.EmailField()
    image = models.ImageField(blank=True, null=True)
    notification = models.BooleanField(default=True,blank=True)
    date = models.DateField(auto_now=True)

    def __str__(self):
        return self.first_name + " " + self.last_name

    @property
    def get_image(self):
        return self.image.url if self.image and hasattr(self.image, 'url') else '/media/avatar.jpg'

    class Meta:
        ordering = ['first_name', 'last_name']


class MpbUser(models.Model):

    genders = (('Male', 'Male'),
               ('Female', 'Female'))
    user = models.OneToOneField(User)
    activation_key = models.CharField(max_length=500, blank=True)
    expiration_date = models.DateTimeField(default=datetime.datetime.now())
    first_name = models.CharField(max_length=25, verbose_name='First name')
    last_name = models.CharField(max_length=25, verbose_name='Last name')
    gender = models.CharField(max_length=6, choices=genders, verbose_name='Gender', blank=True, null=True)
    birth_date = models.DateField(blank=True, null=True, verbose_name='Birth date')
    image = models.ImageField(blank=True, null=True, verbose_name='Profile image')
    phone_number = models.CharField(max_length=16, blank=True, null=True, verbose_name='Phone number')
    cellular_number = models.CharField(max_length=16, blank=True, null=True, verbose_name='Cellular number')
    email = models.EmailField(verbose_name='Email')
    date = models.DateField(auto_now=True)
    notification = models.BooleanField(default=True)

    class Meta:
        ordering = ['first_name', 'last_name']

    def __str__(self):
        return self.first_name + " " + self.last_name

    def save(self, *args, **kwargs):
        super(MpbUser, self).save(*args, **kwargs)

    @property
    def get_image(self):
        return self.image.url if self.image and hasattr(self.image, 'url') else '/media/defaultavatar.jpg'


# Userin xal verdiyi.Raiting
class FavoriteChurch(models.Model):
    POINT_CHOICES = ((1, 1), (2, 2), (3, 3), (4, 4), (5, 5))
    user = models.ForeignKey(MpbUser, to_field='id')
    church = models.ForeignKey(Church, to_field='id')
    date = models.DateField(auto_now=True)
    point = models.IntegerField(choices=POINT_CHOICES)


# Userin sadece beyendikleri


class LikeChurch(models.Model):
    user = models.ForeignKey(MpbUser)
    church = models.ForeignKey(Church)
    date = models.DateField(auto_now=True)



class MpbAddress(models.Model):
    owner = models.OneToOneField(MpbUser)
    country = models.ForeignKey(Country, null=True, blank=True)
    state = models.ForeignKey(State, null=True, blank=True)
    city = models.ForeignKey(City, null=True, blank=True)
    street = models.CharField(max_length=60, null=True, blank=True)


class PastorAddress(models.Model):
    owner = models.OneToOneField(Pastor)
    country = models.ForeignKey(Country, null=True, blank=True)
    state = models.ForeignKey(State, null=True, blank=True)
    city = models.ForeignKey(City, null=True, blank=True)
    street = models.TextField(max_length=100, null=True, blank=True)


class ChurchAddress(models.Model):
    owner = models.OneToOneField(Church)
    country = models.ForeignKey(Country, blank=True)
    state = models.ForeignKey(State, blank=True)
    city = models.ForeignKey(City, blank=True)
    street = models.CharField(max_length=100, blank=True)


class RequestText(models.Model):
    STATUS_CHOICE = (
        ('with_money', 'with_money'),
        ('without_money', 'without_money')
    )
    user = models.ForeignKey(MpbUser, to_field='id')
    title = models.CharField(max_length=150, blank=True, verbose_name='Title')
    text = models.TextField(max_length=320, verbose_name='Request text')
    type = models.CharField(max_length=20, choices=STATUS_CHOICE)
    date = models.DateField(auto_now=True)
    status = models.CharField(max_length=12, default='pending', verbose_name="Status")
    church = models.ForeignKey(Church, verbose_name="Church", null=True, blank=True)

    def __str__(self):
        return self.text


class RequestSound(models.Model):
    STATUS_CHOICE = (
        ('with_money', 'with_money'),
        ('without_money', 'without_money')
    )
    user = models.ForeignKey(MpbUser, to_field='id')
    title = models.CharField(max_length=15, blank=True, null=True)
    sound = models.FileField(upload_to='files/sound')
    type = models.CharField(max_length=20, choices=STATUS_CHOICE)
    date = models.DateField(auto_now=True)
    status = models.CharField(max_length=12, default='pending', verbose_name="Status")
    church = models.ForeignKey(Church, verbose_name="Church", null=True, blank=True)


class ChurchRequest(models.Model):
    STATUS_CHOICE = (
        ('pending', 'pending'),
        ('view', 'view'),
        ('confirmed', 'confirmed'),
        ('cancel', 'cancel'),
        ('responded', 'responded'),
    )
    church = models.ForeignKey(Church, to_field='id')
    request = models.ForeignKey(RequestText)
    status = models.CharField(max_length=10, choices=STATUS_CHOICE, default='pending')
    date = models.DateField(auto_now=True)

    def __str__(self):
        return self.status + "|" + str(self.church)

    def save(self, *args, **kwargs):
        super(ChurchRequest, self).save(*args, **kwargs)
        cancel_count = ChurchRequest.objects.filter(request=self.request, status='cancel').count()
        confirm_count = ChurchRequest.objects.filter(request=self.request, status='confirmed').count()
        all_count = ChurchRequest.objects.filter(request=self.request).count()
        if all_count == cancel_count:
            req = RequestText.objects.filter(text=self.request.text)[0]
            req.status = "cancel"
            req.save()
        if confirm_count == 1:
            req = RequestText.objects.filter(text=self.request.text)[0]
            req.status = "confirmed"
            req.church = self.church
            req.save()

    # def save(self, *args, **kwargs):
    #     pastors = Pastor.objects.filter(church=self.church)
    #     for p in pastors:
    #         if p.notification == True:
    #             p.user.email_user("mesaj","{link}".format(link="/".join([settings.HOST,"myprayerbox/pastor_messages"])),
    #                               from_email=DEFAULT_FROM_EMAIL, fail_silently=False)
    #     super(ChurchRequest, self).save(*args, **kwargs)


class ChurchRequestSound(models.Model):
    STATUS_CHOICE = (
        ('pending', 'pending'),
        ('view', 'view'),
        ('confirmed', 'confirmed'),
        ('cancel', 'cancel'),
        ('responded', 'responded'),
    )
    church = models.ForeignKey(Church, to_field='id')
    request = models.ForeignKey(RequestSound)
    status = models.CharField(max_length=10, choices=STATUS_CHOICE, default='pending')
    date = models.DateField(auto_now=True)

    def __str__(self):
        return self.status + "|" + str(self.church)

    class Meta:
        unique_together = ['church', 'request']
# Kilsenin (pastorun)cavabi


class Response(models.Model):
    pastor = models.ForeignKey(Pastor, to_field='id')
    church = models.ForeignKey(Church, to_field='id')
    request = models.OneToOneField(ChurchRequest)
    text = models.TextField(verbose_name='Response text')
    sound_response = models.FileField(blank=True, null=True, upload_to = "files/sound")
    date = models.DateField(auto_now=True)

    class Meta:
        unique_together = ['church', 'request']


class DropAccount(models.Model):
    user = models.OneToOneField(MpbUser, unique=True, to_field='id')
    reason = models.TextField(max_length=100, null=True, blank=True)
    date = models.DateField(auto_now=True)


class Transaction(models.Model):
    user = models.ForeignKey(MpbUser, to_field='id')
    status = models.CharField(max_length=20)
    log = models.TextField(max_length=200)
    date = models.DateField(auto_now=True)


class BlackList(models.Model):
    church = models.ForeignKey(Church, to_field='id')
    user = models.ForeignKey(MpbUser, to_field='id')
    reason = models.TextField(max_length=200)
    date = models.DateField(auto_now=True)

    class Meta:
        unique_together = ['church', 'user']


class BuyToken(models.Model):
    user = models.ForeignKey(MpbUser, to_field='id')
    token = models.ForeignKey(Token)
    date = models.DateField(auto_now=True)


class SpendToken(models.Model):
    user = models.ForeignKey(MpbUser, to_field='id')
    request_value = models.PositiveIntegerField(blank=True)
    date = models.DateField(auto_now=True)

    def save(self, *args, **kwargs):
        r = RequestValue.objects.get(status=True)
        self.request_value = r.value
        super(SpendToken, self).save(*args, **kwargs)


#Gundelik nece request gondermek olar table

class RequestCount(models.Model):
    count = models.PositiveIntegerField(unique=True)
    status = models.NullBooleanField(blank=True)
    date = models.DateField(auto_now=True)

    def __str__(self):
        return str(self.count)

    def save(self, *args, **kwargs):
        RequestCount.objects.filter(status=True).update(status=False)
        super(RequestCount, self).save(*args, **kwargs)

    class Meta:
        ordering = ['count']


# pulsuz request gonderilen zaman secilen kilselerin sayi
class RandomChurchCount(models.Model):
    count = models.PositiveIntegerField(unique=True)
    status = models.NullBooleanField(blank=True)
    date = models.DateField(auto_now=True)

    def __str__(self):
        return str(self.count)

    def save(self, *args, **kwargs):
        RandomChurchCount.objects.filter(status=True).update(status=False)
        super(RandomChurchCount, self).save(*args, **kwargs)

    class Meta:
        ordering = ['count']


class Help(models.Model):
    question = models.TextField()
    answer = models.TextField()


class Privacy(models.Model):
    content = models.TextField()

class Terms(models.Model):
    content = models.TextField()
