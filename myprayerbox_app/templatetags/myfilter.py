from django import template
from myprayerbox_app.models import Church, FavoriteChurch, ChurchRequest, MpbUser, RequestText, RequestCount, LikeChurch, Pastor, BlackList
from datetime import date
register = template.Library()

@register.filter
def rate(church):
    c_f = FavoriteChurch.objects.filter(church=church)
    c = FavoriteChurch.objects.filter(church=church).count()
    s = 0
    for i in c_f:
        s = (s + i.point)/c
    return s


@register.filter
def req_status(req):
    count_all = ChurchRequest.objects.filter(request=req).count()
    count_cancel = ChurchRequest.objects.filter(request=req, status='cancel').count()
    count_confirm = ChurchRequest.objects.filter(request=req, status='confirmed').count()
    if count_all == count_cancel:
        return "cancelled"
    elif count_confirm == 1:
        return "confirmed"
    elif count_confirm == 0:
        return "pending"

# Userin gun erzin gonderdiyi messeage-larin sayini gosterir
@register.filter
def req_count(user):
    mpbuser = MpbUser.objects.get(user=user)
    user_messeage_count = RequestText.objects.filter(user=mpbuser, date=date.today()).count()
    req_count = RequestCount.objects.get(status=True).count
    return "{} / {}".format(user_messeage_count, req_count)


@register.filter
def confirm_church(req):
    try:
        church = ChurchRequest.objects.get(request=req, status='confirmed').church
    except:
        return ""
    return church

@register.filter
def church(req):
    church = ChurchRequest.objects.filter(request=req)[0].church
    id = church.id
    return id

@register.filter
def isconfirmed(req):
    ch_req = ChurchRequest.objects.filter(request=req.request, status='confirmed').count()
    return ch_req

@register.filter
def church_like_count(church):
    count = LikeChurch.objects.filter(church=church).count()
    return count

@register.filter
def like_count_user(church,user):
    mpbuser = MpbUser.objects.get(user=user)
    like_count = LikeChurch.objects.filter(user=mpbuser, church=church).count()
    return like_count

@register.filter
def isuser_pastor(user):
    if MpbUser.objects.filter(user=user):
        return True
    else:
        return False


@register.filter
def confirmed_count(user):
    church = Pastor.objects.get(user=user).church
    count = ChurchRequest.objects.filter(church=church, status='confirmed').count()
    return count


@register.filter
def confirmed_count_mpbuser(user):
    count = 0
    mpbuser = MpbUser.objects.get(user=user)
    requests = RequestText.objects.filter(user=mpbuser)
    for req in requests:
        if ChurchRequest.objects.filter(request=req, status='confirmed'):
            count += 1

    return count

@register.filter
def isblacklist(id):
    mpbuser = MpbUser.objects.get(id=id)
    if BlackList.objects.filter(user=mpbuser):
        return True
    else:
        return False

@register.filter
def point(user,church):
    church = Church.objects.get(id=church)
    point = FavoriteChurch.objects.filter(user=user,church=church)[0].point
    return points
