import hashlib
import datetime
import random
from django.utils import timezone
from django.conf import settings
from django.core.urlresolvers import reverse


def send_activation(user):
    salt = hashlib.sha1(str(random.random()).encode("utf-8")).hexdigest()[:10]
    key = hashlib.sha256(str(user.user.username + salt).encode("utf-8")).hexdigest()
    user.activation_key = key
    user.expiration_date = timezone.now() + datetime.timedelta(days=1)
    user.save()

    user.user.email_user(
        "Confirmation of registration",
        """Thank you for registration,
        please click the following link in order to confirm your registration:
        {link}?activation={key}
        """.format(link="".join([settings.HOST, reverse("confirm")]), key=key),
        "no-reply@prayer.com",
        fail_silently=False
    )