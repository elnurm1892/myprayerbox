from django.core.mail import EmailMultiAlternatives
from django.shortcuts import render, render_to_response, redirect, get_object_or_404
from django.db.models import Q
from myprayerbox_app.models import *
from myprayerbox_app.forms import *
from django.http import HttpResponseRedirect, HttpResponse, Http404, HttpResponseBadRequest
from django.template import RequestContext
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth import *
from django.http import HttpResponseForbidden, JsonResponse
from django.template import RequestContext
from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse
from django.utils import timezone
from django.views.generic import View
from django.db.utils import IntegrityError
from random import randrange
from myprayerbox_app.tools import send_activation
from django import forms as django_forms
from datetime import date
from myprayerbox_pro.settings import *
from django.template.loader import get_template
from django.template import Context
from myprayerbox_app.templatetags.myfilter import req_status
from django.core.paginator import *


def myprayerbox(request):
    return HttpResponse("Ana sehife")


def help_index(request):
    helps = Help.objects.all()
    return render_to_response('help-index.html', locals(), context_instance=RequestContext(request))


def help(request):
    if request.user.is_authenticated():
        helps = Help.objects.all()
        return render_to_response('help.html', locals(), context_instance=RequestContext(request))
    else:
        return redirect(reverse("login"))


def privacy_index(request):
    privacys = Privacy.objects.all()
    if privacys:
        privacy = Privacy.objects.all()[0]
    else:
        privacy = {}
    return render_to_response('privacy-index.html', locals(), context_instance=RequestContext(request))


def privacy(request):
    if request.user.is_authenticated():
        privacys = Privacy.objects.all()
        if privacys:
            privacy = Privacy.objects.all()[0]
        else:
            privacy = {}
        return render_to_response('privacy.html', locals(), context_instance=RequestContext(request))
    else:
        return redirect(reverse('login'))


def terms_index(request):
    term = Terms.objects.all()
    if term:
        terms = Terms.objects.all()[0]
    else:
        terms = {}
    return render_to_response('terms-index.html', locals(), context_instance=RequestContext(request))


def terms(request):
    if request.user.is_authenticated():
        term = Terms.objects.all()
        if term:
            terms = Terms.objects.all()[0]
        else:
            terms = {}
        return render_to_response('terms.html', locals(), context_instance=RequestContext(request))
    else:
        return redirect(reverse("login"))


def logout_site(request):
    logout(request)
    return redirect(reverse('login'))


def home(request):
    search_form = SearchForm()
    if request.GET.get('key_word'):

        search_form = SearchForm(request.GET)
        if search_form.is_valid():
            key = search_form.cleaned_data['key_word']
            churchs = Church.objects.filter(name__icontains=key)

    if request.method == 'GET':
        if request.user.is_authenticated() and MpbUser.objects.filter(user=request.user):
            mpb = MpbUser.objects.get(user=request.user)
        elif request.user.is_authenticated() and Pastor.objects.filter(user=request.user):
            return redirect(reverse("pastor_inbox"))
        else:
            return redirect(reverse("login"))

    if request.GET.get('logout'):
        logout(request)
        return redirect(reverse("login"))
    return render_to_response('myprayerbox.html', locals(), context_instance=RequestContext(request))


def login_site(request):
    username = ''
    state = ''
    if request.method == 'GET':
        if request.user.is_authenticated() and MpbUser.objects.filter(user=request.user):
            return redirect(reverse("messages"))
        elif request.user.is_authenticated() and Pastor.objects.filter(user=request.user):
            return redirect(reverse('pastor_inbox'))

    if request.POST.get('login'):
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(username=username, password=password)
        if user is not None:
            if user.is_active:
                login(request, user)
                if MpbUser.objects.filter(user=user):
                    return redirect(reverse("home"))
                elif Pastor.objects.filter(user=user):
                    return redirect(reverse('pastor_inbox'))
            else:
                state = "Your account is not active, please contact the MPB admin."
        elif not username or not password:
            state = "Username and/or password is empty"
        else:
            state = "Your username and/or password were incorrect."

    return render_to_response('index.html', {'state': state, 'username': username},
                              context_instance=RequestContext(request))


class UpdateProfile(View):
    def get(self, request):
        if request.user.is_authenticated():
            mpbuser = MpbUser.objects.get(user=request.user)
            update_form = MpbUserForm(instance=mpbuser)
            try:
                address_ob = MpbAddress.objects.get(owner=mpbuser)
                address_form = AddressForm(instance=address_ob)
            except:
                address_form = AddressForm()
            return render_to_response("myprofile.html", locals(), context_instance=RequestContext(request))
        else:
            return redirect(reverse("login"))

    def post(self, request):
        if request.user.is_authenticated():
            mpbuser = MpbUser.objects.get(user=request.user)
            update_form = MpbUserForm(request.POST, request.FILES, instance=mpbuser)
            try:
                address_ob = MpbAddress.objects.get(owner=mpbuser)
                address_form = AddressForm(request.POST, instance=address_ob)
            except:
                address_form = AddressForm(request.POST)
            if update_form.is_valid() or address_form.is_valid():
                update_form.save()
                user = User.objects.get(username=request.user)
                user.first_name = update_form.cleaned_data['first_name']
                user.last_name = update_form.cleaned_data['last_name']
                user.email = update_form.cleaned_data['email']
                user.username = user.email
                user.save()
                address = address_form.save(commit=False)
                address.owner = mpbuser
                address.save()
                return redirect(reverse("update_profile"))
        else:
            return redirect(reverse("login"))


def pastor(request):
    try:
        pastor = Pastor.objects.get(user=request.user)
    except:
        return Http404
    pastor_form = PastorForm(instance=pastor)
    if request.method == 'POST':
        pastor = Pastor.objects.get(user=request.user)
        pastor_form = PastorForm(request.POST, request.FILES, instance=pastor)
        pastor_form.image = request.FILES.get('image')
        pastor_form.notification = request.POST.get('notification')
        pastor_form.save()
        return redirect(reverse("pastor_profile"))
    search_form = SearchForm()
    if request.GET.get('key_word'):
        search_form = SearchForm(request.GET)
        if search_form.is_valid():
            key = search_form.cleaned_data['key_word']
            mpbusers = MpbUser.objects.filter(first_name__contains=key)
            church = Church.objects.filter(name__contains=key)
    if request.method == 'GET':
        if request.user.is_authenticated() and Pastor.objects.filter(user=request.user):
            pastor = Pastor.objects.get(user=request.user)
        else:
            return render_to_response('404.html')

    if request.GET.get('logout'):
        logout(request)
        return redirect(reverse("login"))
    return render_to_response('pastor-profile.html', locals(), context_instance=RequestContext(request))


def change_password(request):
    state = ''

    if request.method == 'POST':
        if request.user.is_authenticated():
            password1 = request.POST.get('password1')
            password2 = request.POST.get('password2')
            if password1 != password2 or not password1 or not password2:
                state = "The passwords are not match"
            elif password1 == password2:
                user = User.objects.get(username=request.user)
                user.set_password(password2)
                user.save()

                return redirect(reverse("login"))
        else:
            return redirect(reverse("login"))
    else:
        if not request.user.is_authenticated():
            return redirect(reverse("login"))

    return render_to_response('settings.html', locals(), context_instance=RequestContext(request))


def church(request, id):
    if request.user.is_authenticated():
        try:
            church = Church.objects.get(id=id)
        except:
            return render_to_response("404.html", context_instance=RequestContext(request))
        rate_form = RateForm()

        # like_count = LikeChurch.objects.filter(user=mpbuser, church=church).count()
        if request.POST.get('point'):
            rate_form = RateForm(request.POST)
            if rate_form.is_valid():
                mpbuser = MpbUser.objects.get(user=request.user)
                point = rate_form.cleaned_data['point']
                if not FavoriteChurch.objects.filter(user=mpbuser, church=church):
                    FavoriteChurch(user=mpbuser, church=church, point=point).save()
                else:
                    r_t = FavoriteChurch.objects.get(user=mpbuser, church=church)
                    r_t.point = rate_form.cleaned_data['point']
                    r_t.save()

        return render_to_response('churchinfo.html', locals(), context_instance=RequestContext(request))
    else:
        return redirect(reverse('login'))


class Churchs(View):
    def get(self, request):
        if request.user.is_authenticated():
            churchs = Church.objects.all()
            return render_to_response('search.html', locals(), context_instance=RequestContext(request))

    def post(self, request):
        if request.user.is_authenticated():
            state = ""
            key = request.POST.get('search')
            k = len(key)
            if k <= 3:
                state = "Less than 3 characters"
                return render_to_response('search.html', locals(), context_instance=RequestContext(request))
            churchs = Church.objects.filter(name__icontains=key)
            return render_to_response('search.html', locals(), context_instance=RequestContext(request))


def drop_account(request):
    state = ""
    drop_account_form = DropAccountForm()
    if request.method == 'GET':
        if not request.user.is_authenticated():
            return redirect(reverse("login"))

    if request.method == 'POST':
        drop_account_form = DropAccountForm(request.POST)
        if drop_account_form.is_valid():
            t_f = drop_account_form.cleaned_data
            if t_f['reason']:
                state = "Reason is empty"
            else:
                mpbuser = MpbUser.objects.get(user=request.user)
                user = User.objects.get(username=request.user)
                DropAccount(user=mpbuser, reason=t_f['reason']).save()
                user.is_active = False
                user.save()
                return HttpResponseRedirect('/myprayerbox/home?logout=logout')

    return render_to_response("delete.html", locals(), context_instance=RequestContext(request))


def new_message(request):
    if request.method == 'POST':
        form = CreateRequestForm(request.POST)
        if form.is_valid():
            random_church_count = RandomChurchCount.objects.get(status=True)
            ran_count = random_church_count.count
            request_count = RequestCount.objects.get(status=True)
            req_count = request_count.count
            mpbuser = MpbUser.objects.get(user=request.user)
            church_count = Church.objects.all().count()
            user_msj_count = RequestText.objects.filter(user=mpbuser, date=date.today()).count()
            t_f = form.cleaned_data
            type = t_f['type']
            if type == 'without_money':
                if user_msj_count < req_count:
                    churches = Church.objects.all()
                    elements = []
                    if church_count <= ran_count:
                        request_text = RequestText(user=mpbuser,
                                                   text=t_f['text'],
                                                   title=t_f['title'],
                                                   type=type,
                        )
                        request_text.save()
                        for i in churches:
                            church_request = ChurchRequest(
                                church=i,
                                request=request_text)
                            church_request.save()
                            pastors = Pastor.objects.filter(church=church_request.church)
                            for p in pastors:
                                if p.notification:
                                    try:
                                        p.user.email_user("mesaj", "{link}".format(
                                            link="/".join([settings.HOST, "myprayerbox/pastor_messages"])),
                                                          from_email=DEFAULT_FROM_EMAIL, fail_silently=False)
                                    except:
                                        return HttpResponse("Connection problem: email or internet connection does not exist.")

                        return render_to_response("inner-empty.html", locals(),
                                                  context_instance=RequestContext(request))


                    else:
                        while len(elements) < ran_count:
                            random_church_element = churches[randrange(0, len(churches))]
                            if random_church_element not in elements:
                                elements.append(random_church_element)
                        request_text = RequestText(user=mpbuser,
                                                   text=t_f['text'],
                                                   title=t_f['title'],
                                                   type=type,
                        )
                        request_text.save()
                        for i in elements:
                            church_request = ChurchRequest(
                                church=i,
                                request=request_text)
                            church_request.save()
                            pastors = Pastor.objects.filter(church=church_request.church)
                            for p in pastors:
                                if p.notification:
                                    try:
                                        p.user.email_user("mesaj", "{link}".format(
                                            link="/".join([settings.HOST, "myprayerbox/pastor_messages"])),
                                                          from_email=DEFAULT_FROM_EMAIL, fail_silently=False)
                                    except:
                                        return HttpResponse("Connection problem: email or internet connection does not exist.")

                        return render_to_response("inner-empty.html", locals(),
                                                  context_instance=RequestContext(request))
                else:
                    return render_to_response("message-limit.html",{"req_count":req_count})
            elif type == 'with_money':
                return render_to_response("inner-empty-error.html", locals(), context_instance=RequestContext(request))
    else:
        if request.user.is_authenticated() and MpbUser.objects.filter(user=request.user):
            form = CreateRequestForm()
        elif request.user.is_authenticated() and Pastor.objects.filter(user=request.user):
            return render_to_response('404.html')
        else:
            return redirect(reverse('login'))
    return render_to_response('newrequest.html', locals(), context_instance=RequestContext(request))


class MessagesView(View):
    def post(self, request):
        if request.user.is_authenticated() and MpbUser.objects.filter(user=request.user):
            def status_validator(status):
                if status not in ['pending', 'view', 'confirmed', 'cancel', 'responded']:
                    raise django_forms.ValidationError('Status is not defined')

            status_field = django_forms.CharField(validators=[status_validator], required=False)
            get_status = request.POST.get('status')
            filt = {'user__user': request.user}
            if get_status:
                filt['status'] = status_field.clean(get_status)

            requests_texts = RequestText.objects.filter(**filt).select_related('user')
            return JsonResponse({'requests': list(dict(
                (req.id, {
                    'id': req.id,
                    'title': req.title,
                    'text': req.text,
                    'date': req.date.strftime('%d/%m/%Y'),
                    'status': req.status,
                    # 'church': {
                    #     'url': reverse('church', args=[req.church.id]),
                    #     'name': req.church.name
                    # }
                }) for req in requests_texts).values())}
            )
        else:
            return redirect(reverse('login'))

    def get(self, request):
        if request.user.is_authenticated() and MpbUser.objects.filter(user=request.user):
            return render_to_response('myprayerbox.html', locals(), context_instance=RequestContext(request))
        else:
            return redirect(reverse('login'))


def message(request, id):
    if request.user.is_authenticated() and MpbUser.objects.filter(user=request.user):
        try:
            message = RequestText.objects.get(id=id, user=MpbUser.objects.get(user=request.user))
        except RequestText.DoesNotExist:
            return render_to_response('404.html', locals())
    else:
        return redirect(reverse('login'))
    return render_to_response('myprayerbox_item.html', locals())

class MessagesViewPastor(View):
    def post(self, request):
        if request.user.is_authenticated():
            def status_validator(status):
                if status not in ['pending', 'view', 'confirmed', 'cancel', 'responded']:
                    raise django_forms.ValidationError('Status is not defined')

            status_field = django_forms.CharField(validators=[status_validator], required=False)
            get_status = request.POST.get('status')
            pastor = Pastor.objects.get(user=request.user)
            filt = {'church': pastor.church}
            if get_status:
                filt['status'] = status_field.clean(get_status)

            church_requests = ChurchRequest.objects.filter(**filt).select_related('request')
            return JsonResponse({'requests': list(dict(
                (ch_req.id, {
                    'id': ch_req.id,
                    'title': ch_req.request.title,
                    'text': ch_req.request.text,
                    'date': ch_req.request.date.strftime('%d/%m/%Y'),
                    'status': ch_req.status
                }) for ch_req in church_requests).values())}
            )
        else:
            return redirect(reverse('login'))

    def get(self, request):
        if request.user.is_authenticated():
            return render_to_response('pastor-inbox.html', locals(), context_instance=RequestContext(request))
        else:
            return redirect(reverse('login'))


def pastor_message(request, ident):
    if request.method == 'GET':
        if request.user.is_authenticated() and Pastor.objects.filter(user=request.user):
            try:
                msg = ChurchRequest.objects.get(id=ident, church=Pastor.objects.get(user=request.user).church)
            except ChurchRequest.DoesNotExist:
                return render_to_response('404.html') 
        else:
            return redirect(reverse('login'))
        return render_to_response('pastor-inbox-item.html', locals(), context_instance=RequestContext(request))
    elif request.method == 'POST':
        if request.user.is_authenticated():
            msg = get_object_or_404(ChurchRequest, id=ident)
            msg_status = ChurchRequest.objects.filter(request=msg.request, status='confirmed')
            if not msg_status:
                try:
                    msg.status = 'confirmed'
                    msg.save()
                    if msg.request.user.notification:
                        msg.request.user.user.email_user("Request confirmed on Myprayerbox.church","""Dear,{user} 
                                                         Your request http://myprayerbox.church/myprayerbox/message/{request}/ has been confirmed by the registred church .
                                                         God bless you!
                                                         Best regards,
                                                         Myprayerbox.church Team""".format(user=msg.request.user.first_name,request=msg.request.id),
                                                         from_email=DEFAULT_FROM_EMAIL, fail_silently=False)

                    return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/'))
                except IntegrityError:
                    return JsonResponse({"message": "Something went wrong"})
            else:
                return JsonResponse({"message": "Confirmed"})
        else:
            return redirect(reverse('login'))


def message_cancel(request, ident):
    if request.method == 'POST':
        msg = get_object_or_404(ChurchRequest, id=ident)
        msg.status = "cancel"
        msg.save()
    return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/'))


class UserRegister(View):
    def get(self, request):
        if request.user.is_authenticated():
            return redirect(reverse('home'))
        else:
            register_form = RegistrationForm()
            return render_to_response('signup.html', locals(), context_instance=RequestContext(request))

    def post(self, request):
        register_form = RegistrationForm(request.POST)
        if register_form.is_valid():
            data = register_form.cleaned_data
            user = User.objects.create_user(
                username=data.get('email'),
                email=data.get('email'),
                password=data.get('password2'),
                first_name=data.get("name"),
                last_name=data.get("surname"),
            )
            user.is_active = False
            user.save()

            mpbuser = MpbUser.objects.create(
                user=user,
                first_name=data.get('name'),
                last_name=data.get("surname"),
                email=data.get('email'),
            )
            send_activation(mpbuser)

            return render_to_response('success_message.html')


        # form yarat
        # formani yoxla
        # auth.models.User yarat
        # MpbUser yarat onun user atributuna uxaridaki user`i menimset
        # send_activation funksiyasinin arqumentine MpbUser obyektini otur
        return render_to_response('signup.html', locals(), context_instance=RequestContext(request))


def confirm(request):
    try:
        activation_key = django_forms.CharField().clean(request.GET.get("activation"))
        confirm_user = MpbUser.objects.get(activation_key=activation_key)
        if confirm_user.user.is_active:
            return HttpResponseForbidden()

        if confirm_user.expiration_date > timezone.now():
            confirm_user.user.is_active = True
            confirm_user.user.save()
            return render_to_response("register-after.html")
        else:
            raise ValueError()

    except (MpbUser.DoesNotExist, ValueError, django_forms.ValidationError):
        return HttpResponseBadRequest()


def like(request, ident):
    if request.user.is_authenticated():
        try:
            mpbuser = MpbUser.objects.get(user=request.user)
            church = Church.objects.get(id=ident)
        except:
            return HttpResponse("User or pastor does not exist.")
        if LikeChurch.objects.filter(user=mpbuser, church=church).count() == 0:
            like = LikeChurch(user=mpbuser, church=church)
            like.save()
        else:
            return HttpResponse("Made as like")
        return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/'))
    else:
        return redirect(reverse('login'))


def unlike(request, ident):
    if request.user.is_authenticated():
        try:
            mpbuser = MpbUser.objects.get(user=request.user)
            church = Church.objects.get(id=ident)
        except:
            return HttpResponse("User or pastor does not exist.")
        like = LikeChurch.objects.get(user=mpbuser, church=church).delete()
    else:
        return redirect(reverse('login'))
    return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/'))


def likechurch(request):
    try:
        mpbuser = MpbUser.objects.get(user=request.user)
    except:
        return HttpResponse("User does not exist.")

    likechurchs = LikeChurch.objects.filter(user=mpbuser)
    return render_to_response("favorites.html", locals(), context_instance=RequestContext(request))


def addblacklist(request, ident):
    mpbuser = MpbUser.objects.get(id=ident)
    church = Pastor.objects.get(user=request.user).church
    if request.method == 'POST':
        if not BlackList.objects.filter(user=mpbuser):
            blck = BlackList(church=church, user=mpbuser, reason=request.POST.get("reason"))
            blck.save()
            return render_to_response("pastor-blacklist-success.html")
        else:
            return render_to_response("pastor-blacklist-unique.html")

    return render_to_response("blacklist.html", locals(), context_instance=RequestContext(request))


def blacklist(request):
    church = Pastor.objects.get(user=request.user).church
    blacklists = BlackList.objects.filter(church=church)
    return render_to_response("pastor-blockedlist.html", locals())


def removeblacklist(request, ident):
    mpbuser = MpbUser.objects.get(id=ident)
    blck = BlackList.objects.get(user=mpbuser)
    blck.delete()

    return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/'))


def rate(request):
    if request.is_ajax():
        if request.method == 'GET':
            message = "fail"
        else:
            user_id = request.POST.get('user')
            rating = request.POST.get('rating')
            ch_id = request.POST.get('ch_id')
            mpbuser = MpbUser.objects.get(id=user_id)
            church = Church.objects.get(id=ch_id)
            if not FavoriteChurch.objects.filter(user=mpbuser, church=church):
                fv = FavoriteChurch(user=mpbuser, church=church, point=int(rating))
                fv.save()
                message = "ok"
            else:
                fv = FavoriteChurch.objects.get(user=mpbuser, church=church)
                fv.point = int(rating)
                fv.save()
                message = "ok"
    else:
        message = "no xhr"
    return HttpResponse(message)


def fv_churchs(request):
    fv_ch = FavoriteChurch.objects.all()
    return JsonResponse({'requests': list(dict(
                (fv.id, {
                    'id': fv.id,
                    'church': fv.church.id,
                    'point': fv.point,
                }) for fv in fv_ch).values())}
            )
