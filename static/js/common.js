$(document).ready(function(){
	//$("html").removeClass("no-js").addClass("js").niceScroll({zindex:"500",cursorcolor:"#2663cd",cursorborder:"1px solid #1c4f7e",touchbehavior:"true",autohidemode:"false"});
	$.fn.alignCenter = function(){
		var marginLeft = Math.max(0, parseInt($(window).width()/2 - $(this).width()/2)) + 'px';
		var marginTop = Math.max(0, parseInt($(window).height()/2 - $(this).height()/2) - 30) + 'px';
		return $(this).css({'margin-left':marginLeft,'margin-top':marginTop});
	};
	resizeWrapper();
	resizeTerms();	
	$(window).resize(function(){
		resizeWrapper();
		sliderCorrect();
		$(".home .wrapper section").alignCenter();
		resizeTerms();
	});
	
	//slider
		var count = $(".slider li").length;
		var currentIndex = 0;
		var sliderTimer;
		
		$(".slider li img").each(function(e){
			var imgWidth = $(this).attr("width");
			var imgHeight = $(this).attr("height");
			$(this).data("width",imgWidth);
			$(this).data("height",imgHeight);
		});	
		
		sliderCorrect();
		
		$(".slider li").not($(".slider li").eq(currentIndex)).fadeOut(0);
		
		function nextSlider(){
			if(currentIndex == count - 1) nextIndex = 0; else nextIndex = currentIndex + 1;
			
			$(".slider li").eq(currentIndex).fadeOut(800);
			$(".slider li").eq(nextIndex).fadeIn(800);
			
			currentIndex = nextIndex;
		}
		
		sliderTimer = setInterval(nextSlider,7000);
		
	// others
	
	$(".home .wrapper section").alignCenter();

	$(".signform .signup").on("click",function(){
		$(".terms_window").css("display","block");
	});
	
	$(".terms_window .accept").on("click",function(){
		//location = '';
	});
	$(".terms_window .decline").on("click",function(){
		$(".terms_window").css("display","none");
	});
	
	$(".home input[type=text],.home input[type=password],textarea,.nr .field input").each(function(){
		$(this).data("defVal",$(this).val());
	});
	
	$(".home input[type=text],.home input[type=password],textarea,.nr .field input").focus(function(){
		if($(this).val() == $(this).data("defVal")) $(this).val('');
	}).blur(function(){
		if($(this).val() == '') $(this).val($(this).data("defVal"));
	});
	
	$(".menu-container .head button.close").on('click',function(){
		$(".menu-container").animate({left:"-360px"},250,function(){$(this).css("display","none")});
	});
	$("header .menu a").on('click',function(){
		$(".menu-container").css("display","block").animate({left:"0",visibility:"visible"},250);
	});
	
	if($("body.inner").length){
		
		$(document).on('click touchstart',function(e){
			if(!$(e.target).closest(".menu-container").length && !$(e.target).closest("li.menu").length){
				$(".menu-container").animate({left:"-360px"},250,function(){$(this).css("display","none")});
			}
		});
	}
	
	if($(".faq").length){
		$(".faq li .question").on("click",function(){
			
			var opened = $(this).closest("li").hasClass("list-opened");
			
			$(".faq li.list-opened").find(".answer").slideUp(300);
			$(".faq li").removeClass("list-opened");
			
			if(!opened){
				$(this).parent().find(".answer").slideDown(300);
				$(this).closest("li").addClass("list-opened");
			}
		});
	}

	var $birthday = $(".option-form input[name=birthday]");
	if($birthday.length){
		$birthday.datepicker({changeMonth:true,changeYear:true,yearRange:"-100:+0"});
	}
//	var $loadmore = $("section.search .result .load-more");
//	if($loadmore.length){
//		$loadmore.on("click",function(){
//			var $churchitem = $("section.search .result .church-item");
//			var $first = $churchitem.first();
//			for(i=1;i<=5;i++){
//				$first.clone().insertAfter($churchitem.last());
//			}
//		});
//	}
	if($("section.inbox").length){
		//$(document.body).on('click','section.inbox .more,section.inbox .message',function(){
		//	var $more = $(this).closest(".inbox-item").find(".more");
		//	var $message = $(this).closest(".inbox-item").find(".message");
		//	var height = $message.get(0).scrollHeight;
//
		//	if(parseInt(height) <= 52){
		//		$more.remove();
		//	}else if($more.hasClass("less")){
		//		$message.animate({height:"52px",overflow:"hidden"},200);
		//		$more.removeClass("less");
		//	}else{
		//		$("section.inbox .message").animate({height:"52px",overflow:"hidden"},200);
		//		$("section.inbox .more").removeClass("less");
		//		$message.animate({height:height,overflow:"visible"},200);
		//		$more.addClass("less");
		//	}
		//});
		$("section.inbox .load-more").on("click",function(){
			var $inbox = $("section.inbox .inbox-item");
			var $first = $inbox.first();
			for(i=1;i<=5;i++){
				$first.clone().insertAfter($inbox.last());
			}
		});
	}

	if($(".back-button button").length){
		$(".back-button button").on("click",function(){
			location = document.referrer;
		});
	}
	if($("body.churchinfo").length){
		$("a.view").lightBox();
	}
	function resizeWrapper(){
		if($("body.inner").length){
			var height = $(window).height();
			var paddingTop = $("body.inner .wrapper").css("padding-top");
			var paddingBottom = $("body.inner .wrapper").css("padding-bottom");
			var minHeight = height-parseInt(paddingTop)-parseInt(paddingBottom);
			$("body.inner .wrapper").css({minHeight:minHeight+"px",paddingTop:paddingTop,paddingBottom:paddingBottom});
		}
	}
	function resizeTerms(){
		var height = parseInt($(window).height());
		var maxHeight = 450;
		var realHeight = 0;
		if(height-150 > 450) realHeight = 450; else realHeight = height-150;
		$(".home .terms_window .terms_content").css("height",realHeight+"px");
		$(".home .terms_window").alignCenter();
	}
	function sliderCorrect(){
		
		$(".slider li img").each(function(e){
			var imgWidth = $(this).data("width");
			var imgHeight = $(this).data("height");

			var winWidth=$(window).width();
			var winHeight=$(window).height();

			var xRatio = imgWidth / imgHeight;
			var yRatio = imgHeight / imgWidth;
			
			if ((winHeight / winWidth) < yRatio) {
				$(this).attr("width",winWidth);
				$(this).attr("height",yRatio*winWidth);
			} else {
				$(this).attr("height",winHeight);
				$(this).attr("width",xRatio*winHeight);
			};

			$(this).css("margin-left",(winWidth-$(this).width())/2);
			$(this).css("margin-top",(winHeight-$(this).height())/2);
		});	
	}
	$(".nr .request-form textarea").on("keyup",function(){
		countChar(this);
	});
	function countChar(val){
		var len = val.value.length;
		if(len > 320){
			val.value = val.value.substring(0, 320);
			len = val.value.length;
		}
		$('.nr .request-form .calc span').text(320 - len);
	}
});