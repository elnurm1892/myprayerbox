$(function(){
    function getMessages(param){
        $.post('/myprayerbox/messages/', {status: param})
        .success(function(data){
            $(".inbox-container").html('');
            data.requests.forEach(function(item){
                $(".inbox-container").append(
                    '<div class=\"inbox-item\" ><div class=\"status status-'+item.status + '\">' + item.status  +
                    '</div><div class=\"date\">' + item.date + '</div><div class=\"message\"><a href="' +
                    '\"><span class=\"message-to\">' +  '</span></a><span class=\"message-to\">' + item.title + ':</span>' +
                    item.text + '</div>'+'<a class="more" href="/myprayerbox/message/'+item.id+'">View</a></div>'
                );
            });

        })
        .error(function(){
            alert("Hamisi bosh sohbetdir -_-");
        }
        );
    }

    $("#filter_m").on("change", function(option){
        getMessages(this.value);
    });

    getMessages();
});

//				<div class="inbox-item">
//					<div class="status status-canceled">Cancelled</div>
//					<div class="date">31/02/2015</div>
//					<div class="message">
//						<a href="./myprayerbox_item.html"><span class="message-to">[New York Church]:</span>msg view</a>
//					</div>
//					<a class="more" href="./myprayerbox_item.html">View</a>
//				</div>